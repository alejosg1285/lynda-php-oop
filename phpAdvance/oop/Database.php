<?php
    /**
     * Created by PhpStorm.
     * User: Alejo Saa G
     * Date: 16/08/2018
     * Time: 10:40
     */

    namespace oop\Database;


    abstract class Database
    {
        abstract public function connection();

        public function disconnect() {
            // TODO implement method
        }
    }

    class MySql extends Database {

        public function connection() {
            // TODO: Implement connection() method.
        }
    }

    $mysql = new MySql();