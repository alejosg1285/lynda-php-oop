<?php
    /**
     * Created by PhpStorm.
     * User: Alejo Saa G
     * Date: 16/08/2018
     * Time: 9:40
     */

    namespace oop\Singleton;


    class Singleton
    {
        protected static $instance;

        public static function getInstance() {
            if (null === static::$instance) {
                static::$instance = new static();
            }

            return static::$instance;
        }

        protected function __construct() { }

        private function __clone() {
            // TODO: Implement __clone() method.
        }

        private function __wakeup() {
            // TODO: Implement __wakeup() method.
        }
    }

    class SingletonChild extends Singleton {
        protected static $instance;
    }

    $obj = Singleton::getInstance();
    var_dump($obj === Singleton::getInstance());

    $anotherObj = SingletonChild::getInstance();
    var_dump($anotherObj === Singleton::getInstance());

    var_dump($anotherObj === SingletonChild::getInstance());