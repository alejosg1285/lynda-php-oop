<?php
    /**
     * Created by PhpStorm.
     * User: Alejo Saa G
     * Date: 20/08/2018
     * Time: 21:20
     */

    function fizzbuzz($limit) {
        $i = 0;
        while ($i < $limit) {
            $yield = null;
            if ($i % 3 === 0) { $yield = 'fizz'; }
            if ($i % 5 === 0) { $yield .= 'buzz'; }
            yield $yield;

            $i++;
        }

        return;
    }