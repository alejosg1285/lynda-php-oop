<?php
    /**
     * Created by PhpStorm.
     * User: Alejo Saa G
     * Date: 12/08/2018
     * Time: 20:37
     */

    namespace Project;

    include 'Generators/genertor.php';
    include 'namespace/Table.php';
    include 'interfaces/Log.php';
    include 'interfaces/Table.php';
    include 'trait/Log.php';
    include 'iterators/BasicIterator.php';
    include 'iterators/FilterRows.php';
    include_once 'Closures/closures.php';

    use iTable;
    use iLog;
    use Logg;
    use Project\Table as ProjectTable;
    use App\Iterators as Iterator;

    class App implements iTable, iLog
    {
        use Logg;

        public static function get() {
            echo "Project.Table.get\n";
        }

        public function save(array $data) {
            return 'foo';
        }

        public function log($message) {
            return $message;
        }
    }

    ProjectTable::get();
    echo '<br>';
    echo (new App())->save([]);
    echo '<br>';
    echo (new App())->log('applying interfaces in php');
    echo '<br>';
    echo (new App())->tlog('applying trait in php');

    echo '<br><strong>Iterators</strong><br>';

    $filePath = './data/data.csv';
    /*$iterator = new Iterator\BasicIterator($filePath);
    $iterator = new Iterator\FilterRows($iterator);

    foreach ($iterator as $i => $row) {
        var_dump($row);
    }*/

    echo '<br><strong>Generators</strong><br>';
    foreach (fizzbuzz(25) as $key => $value) {
        echo "{$key} => {$value} <br>";
    }

    echo '<br><strong>Hash</strong><br>';
    include_once 'hash/hash.php';

    echo '<br><strong>Closures</strong><br>';
    $var = 'closure';
    echo $func($var);
    echo $funcUse();
    echo "{$varUse}<br>";
    var_dump($out);
    var_dump($outFilter);
    var_dump($arrInt);