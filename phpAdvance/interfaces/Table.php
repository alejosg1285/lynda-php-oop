<?php
    /**
     * Created by PhpStorm.
     * User: Alejo Saa G
     * Date: 13/08/2018
     * Time: 22:10
     */

    interface iTable
    {
        public function save(array $data);
    }