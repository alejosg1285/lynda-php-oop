<?php
    /**
     * Created by PhpStorm.
     * User: Alejo Saa G
     * Date: 13/08/2018
     * Time: 22:22
     */

    interface iLog
    {
        public function log($message);
    }