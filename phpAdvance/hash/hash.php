<?php
    /**
     * Created by PhpStorm.
     * User: Alejo Saa G
     * Date: 20/08/2018
     * Time: 21:41
     */
    $passwd = password_hash('testing', PASSWORD_DEFAULT);
    echo "{$passwd} <br>";
    $match = password_verify('foo', $passwd);
    echo "{$match} <br>";
    $match = password_verify('testing', $passwd);
    echo "{$match} <br>";
    if (password_needs_rehash($passwd, PASSWORD_DEFAULT, ['cost' => 12])) {
        $passwd = password_hash('testing', PASSWORD_DEFAULT, ['cost' => 12]);
        echo "{$passwd} <br>";
    }