<?php
    /**
     * Created by PhpStorm.
     * User: Alejo Saa G
     * Date: 20/08/2018
     * Time: 21:00
     */

    namespace App\Iterators;


    class FilterRows extends \FilterIterator
    {
        public function accept() {
            // TODO: Implement accept() method.
            $current = $this->getInnerIterator()->current();

            if (count($current) === 1) {
                return false;
            }

            return true;
        }
    }