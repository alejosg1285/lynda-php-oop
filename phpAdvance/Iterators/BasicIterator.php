<?php
    /**
     * Created by PhpStorm.
     * User: Alejo Saa G
     * Date: 20/08/2018
     * Time: 20:32
     */

    namespace App\Iterators;


    class BasicIterator extends \IteratorIterator
    {
        public function __construct($pathToFile) {
            parent::construct(new \SplFileObject($pathToFile, 'r'));

            $file = $this->getInnerIterator();
            $file->setFlags(\SplFileObject::READ_CSV);
            $file->setCsvControl(',', '"', "\\");
        }
    }