<?php
    /**
     * Created by PhpStorm.
     * User: Alejo Saa G
     * Date: 21/08/2018
     * Time: 21:53
     */

    namespace App\Closures;

    $func = function ($string) {
        echo "{$string} called<br>";
    };

    $varUse = 'closure';
    $funcUse = function () use ($varUse) {
        echo "{$varUse} called<br>";
        $varUse = 'function';
    };

    $arrInt = [1, 2, 3, 4, 5, 6, 7, 8, 9];
    $out = array_filter($arrInt, function ($item) {
        return ($item % 2 === 0);
    });

    $funcFilter = function ($item) {
        return ($item % 2 === 1);
    };
    $outFilter = array_filter($arrInt, $funcFilter);