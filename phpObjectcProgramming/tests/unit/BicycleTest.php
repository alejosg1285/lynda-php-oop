<?php
    require 'Bicycle.php';


    use App\Bicycle\Bicycle;

    class BicycleTest extends \Codeception\Test\Unit
    {
        /**
         * @var \UnitTester
         */
        protected $tester;

        protected function _before() {
        }

        protected function _after() {
        }

        // tests
        public function testSomeFeature() {
            $bike = new Bicycle('Totto', '201x', 2018, 'Nuevos productos de Totto', 4.5);

            $this->assertEquals('Totto-v201x-2018', $bike->getName());

            $lbs = 4.5 * 2.2046226218;
            $this->assertEquals($lbs, $bike->getWeightLbs());
            $kg = 5.5 * 2.2046226218;
            $bike->setWeightLbs($kg);
            $this->assertEquals($kg, $bike->getWeightLbs());
        }
    }