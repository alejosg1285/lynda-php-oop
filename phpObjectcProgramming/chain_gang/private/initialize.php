<?php
    ob_start(); // turn on output buffering

    // session_start(); // turn on sessions if needed

    // Assign file paths to PHP constants
    // __FILE__ returns the current path to this file
    // dirname() returns the path to the parent directory
    define("PRIVATE_PATH", dirname(__FILE__));
    define("PROJECT_PATH", dirname(PRIVATE_PATH));
    define("PUBLIC_PATH", PROJECT_PATH . '/public');
    define("SHARED_PATH", PRIVATE_PATH . '/shared');

    // Assign the root URL to a PHP constant
    // * Do not need to include the domain
    // * Use same document root as webserver
    // * Can set a hardcoded value:
    // define("WWW_ROOT", '/~kevinskoglund/chain_gang/public');
    // define("WWW_ROOT", '');
    // * Can dynamically find everything in URL up to "/public"
    $public_end = strpos($_SERVER['SCRIPT_NAME'], '/public') + 7;
    $doc_root = substr($_SERVER['SCRIPT_NAME'], 0, $public_end);
    define("WWW_ROOT", $doc_root);

    require_once('functions.php');
    require_once 'status_error_functions.php';
    require_once 'dbConnection.php';
    require_once 'databaseFunctions.php';
    require_once 'validation_functions.php';

    // Load class definitions manually

    // Autoload class definitions
    spl_autoload_register(function ($className) {
        $arrNames = explode('\\', $className);
        $className = end($arrNames);
        /////if (strcmp($className, 'DatabaseObject') !== 0) {
            if (preg_match('/\A\w+\Z/', $className)) {
                $dirs = ['../private/classes/', '../../../private/classes/', '../../private/classes/'];

                foreach ($dirs as $dir) {
                    if (file_exists("{$dir}{$className}.php")) {
                        include_once "{$dir}{$className}.php";

                        return;
                    }
                }
            }
        ////}
    });

    $database = dbConnect();
    \ChainHang\DatabaseObject::setConnection($database);

    $session = new \ChainHang\Session();
?>
