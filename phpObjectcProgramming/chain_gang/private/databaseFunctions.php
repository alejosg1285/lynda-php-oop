<?php
    /**
     * Created by PhpStorm.
     * User: Alejo Saa G
     * Date: 08/09/2018
     * Time: 19:12
     */

    function dbConnect() {
        $connect = new mysqli(DB_SERVER, DB_USER, DB_PASSWD, DB_NAME);
        confirmDbConnect($connect);

        return $connect;
    }

    function confirmDbConnect($conect) {
        if ($conect->connect_errno) {
            $msg = "Database connection failed: {$conect->connect_error} ({$conect->connect_errno})";
            exit($msg);
        }
    }

    function dbDisconnect($connect) {
        if (isset($connect)) {
            $connect->close();
        }
    }