<?php
    /**
     * Created by PhpStorm.
     * User: Alejo Saa G
     * Date: 26/09/2018
     * Time: 22:08
     */

    namespace ChainHang;


    class Session
    {
        private $adminId;
        public $userName;
        private $lastLogin;

        const MAX_LOGIN_AGE = 86400; //60 * 60 * 24;

        /**
         * Session constructor.
         */
        public function __construct() {
            session_start();
            $this->checkStoredLogin();
        }


        public function login($admin) {
            if ($admin) {
                session_regenerate_id(true);

                $_SESSION['admin_id'] = $admin->id;
                $this->adminId = $admin->id;

                $this->userName = $_SESSION['userName'] = $admin->username;
                $this->lastLogin = $_SESSION['lastLogin'] = time();
            }

            return true;
        }

        public function isLoggedIn() {
            return isset($this->adminId) && $this->lastLoginRecent();
        }

        public function logout() {
            unset($_SESSION['admin_id']);
            unset($this->adminId);
            unset($_SESSION['userName']);
            unset($this->userName);
            unset($_SESSION['lastLogin']);
            unset($this->lastLogin);

            return true;
        }

        private function checkStoredLogin() {
            if (isset($_SESSION['admin_id'])) {
                $this->adminId = $_SESSION['admin_id'];
                $this->userName = $_SESSION['userName'];
                $this->lastLogin = $_SESSION['lastLogin'];
            }
        }

        private function lastLoginRecent() {
            if (!isset($this->lastLogin)) {
                return false;
            } elseif (($this->lastLogin + self::MAX_LOGIN_AGE) < time()) {
                return false;
            } else {
                return true;
            }
        }

        public function message($msg = '') {
            // If $msg is empty, then is a "set" message to session variable, but is empty, then is a "get" message
            // from session variable.
            if (!empty($msg)) {
                $_SESSION['message'] = $msg;
                return true;
            } else {
                return !empty($_SESSION['message']) ? $_SESSION['message'] : '';
            }
        }

        public function clearMessage() {
            unset($_SESSION['message']);
        }
    }