<?php
    /**
     * Created by PhpStorm.
     * User: Alejo Saa G
     * Date: 01/10/2018
     * Time: 21:06
     */

    namespace ChainHang;


    class Pagination
    {
        public $currentPage;
        public $perPage;
        public $totalCount;

        /**
         * Pagination constructor.
         *
         * @param $currentPage
         * @param $perPage
         * @param $totalCount
         */
        public function __construct($currentPage = 1, $perPage = 20, $totalCount = 0) {
            $this->currentPage = (int)$currentPage;
            $this->perPage = (int)$perPage;
            $this->totalCount = (int)$totalCount;
        }

        public function offset() {
            return $this->perPage * ($this->currentPage - 1);
        }

        public function totalPages() {
            return ceil($this->totalCount / $this->perPage);
        }

        public function previousPage() {
            $prev = $this->currentPage - 1;
            return ($prev > 0) ? $prev : false;
        }

        public function nextPage() {
            $next = $this->currentPage + 1;
            return ($next <= $this->totalPages()) ? $next : false;
        }

        public function previousLink($url = '') {
            $link = '';
            if ($this->previousPage() != false) {
                $link = "<a href=\"{$url}?page={$this->previousPage()}\">&laquo; Previous</a>";
            }

            return $link;
        }

        public function nextLink($url = '') {
            $link = '';
            if ($this->nextPage() != false) {
                $link = "<a href=\"{$url}?page={$this->nextPage()}\">Next &raquo;</a>";
            }

            return $link;
        }

        public function numberLinks($url = '') {
            $output = '';
            for ($i = 1; $i <= $this->totalPages(); $i++) {
                if ($i === $this->currentPage)
                    $output .= "<span class=\"selected\">{$i}</span>";
                else
                    $output .= "<a href=\"{$url}?page={$i}\">{$i}</a>";
            }

            return $output;
        }

        public function paginationLinks($url = '') {
            $output = '';
            if ($this->totalPages() > 1) {
                $output .= '<div class="pagination">';
                $output .= $this->previousLink($url);

                $output .= $this->numberLinks($url);

                $output .= $this->nextLink($url);
                $output .= '</div>';
            }

            return $output;
        }
    }