<?php
    namespace ChainHang;

    class DatabaseObject
    {
        static protected $conection;
        static protected $tableName = '';
        static protected $columns = [];
        public $errors = [];

        public static function setConnection($conect) {
            self::$conection = $conect;
        }

        public static function find_by_sql($sql) {
            $result = self::$conection->query($sql);

            if (!$result) {
                exit('Database query fail.');
            }

            // Convert results into objects.
            $arrObject = [];
            while ($record = $result->fetch_assoc()) {
                $arrObject[] = static::instantiate($record);
            }
            $result->free();

            return $arrObject;
        }

        public static function find_all() {
            $sql = 'SELECT * FROM ' . static::$tableName;

            return self::find_by_sql($sql);
        }

        public static function count_all() {
            $sql = 'SELECT COUNT(1) FROM ' . static::$tableName;

            $resultSet = self::$conection->query($sql);
            $row = $resultSet->fetch_array();

            return array_shift($row);
        }

        public static function find_by_id($id) {
            $sql = 'SELECT * FROM ' . static::$tableName . " WHERE id = '" . self::$conection->escape_string($id) . "'";

            $arrOjbect = static::find_by_sql($sql);

            if (!empty($arrOjbect)) {
                return array_shift($arrOjbect);
            } else {
                return false;
            }
        }

        public static function instantiate($record) {
            $object = new static;

            foreach ($record as $property => $value) {
                if (property_exists($object, $property)) {
                    $object->$property = $value;
                }
            }

            return $object;
        }

        protected function validate() {
            $this->errors = [];

            // Custom validations

            return $this->errors;
        }

        protected function create() {
            $this->validate();
            if (!empty($this->errors)) {
                return false;
            }

            $attrs = $this->sanitizedAttributes();

            //$sql = "INSERT INTO bicycles (" . join(', ', self::$dbColumns) . ") ";
            $sql = 'INSERT INTO ' . static::$tableName . ' (' . join(', ', array_keys($attrs)) . ") ";
            $sql .= "VALUES ('" . join("', '", array_values($attrs)) . "')";

            $result = self::$conection->query($sql);

            if ($result) {
                $this->id = self::$conection->insert_id;
            }

            return $result;
        }

        protected function update() {
            $this->validate();
            if (!empty($this->errors)) {
                return false;
            }

            $attrs = $this->sanitizedAttributes();
            $attrPairs = [];

            foreach ($attrs as $key => $value) {
                $attrPairs[] = "{$key} = '{$value}'";
            }

            $sql = 'UPDATE ' . static::$tableName . ' SET ' . join(', ', $attrPairs);
            $sql .= ' WHERE id = ' . self::$conection->escape_string($this->id) . ' LIMIT 1';

            $result = self::$conection->query($sql);

            return $result;
        }

        public function delete() {
            $sql = 'DELETE FROM ' . static::$tableName . " WHERE id = '" . self::$conection->escape_string($this->id) . "' LIMIT 1";

            return self::$conection->query($sql);
        }

        public function save() {
            if (isset($this->id)) {
                return $this->update();
            } else {
                return $this->create();
            }
        }

        public function attributes() {
            $attributes = [];

            foreach (static::$dbColumns as $column) {
                if (strcmp($column, 'id') === 0) {
                    continue;
                }

                $attributes[$column] = $this->$column;
            }

            return $attributes;
        }

        protected function sanitizedAttributes() {
            $sanitized = [];

            foreach ($this->attributes() as $key => $value) {
                $sanitized[$key] = self::$conection->escape_string($value);
            }

            return $sanitized;
        }

        public function mergeAttributes($args = []) {
            foreach ($args as $key => $value) {
                if (property_exists($this, $key) && !is_null($value)) {
                    $this->$key = $value;
                }
            }
        }
    }