<?php
    /**
     * Created by PhpStorm.
     * User: Alejo Saa G
     * Date: 02/09/2018
     * Time: 18:06
     */

    namespace ChainHang;


    class Bicycle extends DatabaseObject
    {
        public $id;
        public $brand;
        public $model;
        public $year;
        public $description;
        public $weight_kg = 0.0;
        public $category;
        public $color;
        public $gender;
        public $price = 0.0;
        private $poundKg = 2.2046226218;
        public $condition_id;

        const CATEGORY = ['Road', 'Mountain', 'Hybric', 'Cruiser', 'BMX'];
        const GENDER = ['Men', 'Women', 'Unisex'];
        const CONDITION = [1 => 'Beat up', 2 => 'Decent', 3 => 'Good', 4 => 'Great', 5 => 'Like new'];

        protected static $tableName = 'bicycles';
        static protected $dbColumns = ['id', 'brand', 'model', 'year', 'category', 'gender', 'color', 'price', 'weight_kg', 'condition_id', 'description'];

        public function __construct($args = []) {
            foreach ($args as $k => $v) {
                if (property_exists($this, $k)) {
                    $this->$k = $v;
                }
            }
        }

        /**
         * Bicycle constructor.
         *
         * @param       $brand
         * @param       $model
         * @param       $year
         * @param       $description
         * @param       $category
         * @param       $color
         * @param       $gender
         * @param float $price
         */
        /*public function __construct($brand, $model, $year, $description, $category, $color, $gender, $price, $condition) {
            $this->brand = $brand;
            $this->model = $model;
            $this->year = $year;
            $this->description = $description;
            $this->category = $category;
            $this->color = $color;
            $this->gender = $gender;
            $this->price = $price;
            $this->conditionId = $condition;
        }*/

        public function getName() {
            return "{$this->brand}-v{$this->model}-{$this->year}";
        }

        public function getWeightKg() {
            return number_format($this->weight_kg, 2) . ' Kg';
        }

        public function setWeightKg($weight = 1.0) {
            $this->weight_kg = $weight;
        }

        public function getWeightLbs() {
            $pounds = floatval($this->weight_kg) / $this->poundKg;

            return number_format($pounds, 2) . ' lbs';
        }

        public function setWeightLbs($weight = 1.0) {
            $this->weight_kg = floatval($weight) * $this->poundKg;
        }

        public function getCondition() {
            return ($this->condition_id > 0 ? self::CONDITION[$this->condition_id] : 'Unknown');
        }

        public function getPrice() {
            return '$' . number_format($this->price, 2);
        }

        protected function validate() {
            $this->errors = [];

            if (is_blank($this->brand)) {
                $this->errors[] = "Brand cannot be blank.";
            }
            if (is_blank($this->model)) {
                $this->errors[] = "Model cannot be blank.";
            }

            return $this->errors;
        }
    }