<?php
    /**
     * Created by PhpStorm.
     * User: Alejo Saa G
     * Date: 03/09/2018
     * Time: 20:59
     */

    namespace ChainHang;


    class ParseCSV
    {
        private $fileName;
        private $header;
        private $data = [];
        public static $delimiter = ',';
        private $rowCount = 0;

        public function __construct($fileName = '') {
            if(!empty($fileName)) {
                $this->file($fileName);
            }
        }

        public function file($fileName) {
            if (!file_exists($fileName)) {
                echo 'File does not exists';
                return false;
            } elseif (!is_readable($fileName)) {
                echo 'File is not readable';
                return false;
            }

            $this->fileName = $fileName;
            return true;
        }

        public function parse() {
            if (!isset($this->fileName)) {
                echo 'File is not set.';
                return false;
            }

            $this->reset();

            $file = fopen($this->fileName, 'r');

            while (!feof($file)) {
                $row = fgetcsv($file, 0, self::$delimiter);

                if ($row == [null] || $row === false) continue;

                if (!$this->header) {
                    $this->header = $row;
                } else {
                    $this->data[] = array_combine($this->header, $row);
                    $this->rowCount++;
                }
            }

            fclose($file);

            return $this->data;
        }

        public function lastResults() {
            return $this->data;
        }

        public function getRowCount() {
            return $this->rowCount;
        }

        private function reset() {
            $this->header = null;
            $this->data = [];
            $this->rowCount = 0;
        }
    }