<?php
    require_once('../private/initialize.php');
    include(SHARED_PATH . '/public_header.php');

    use ChainHang\Bicycle;
    use ChainHang\ParseCSV;

    $page_title = 'Inventory';
?>

<div id="main">

    <div id="page">
        <div class="intro">
            <img class="inset" src="<?php echo url_for('/images/AdobeStock_55807979_thumb.jpeg') ?>"/>
            <h2>Our Inventory of Used Bicycles</h2>
            <p>Choose the bike you love.</p>
            <p>We will deliver it to your door and let you try it before you buy it.</p>
        </div>

        <table id="inventory">
            <tr>
                <th>Brand</th>
                <th>Model</th>
                <th>Year</th>
                <th>Category</th>
                <th>Gender</th>
                <th>Color</th>
                <th>Weight</th>
                <th>Condition</th>
                <th>Price</th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
            <?php
                $parser = new ParseCSV(PRIVATE_PATH.'/used_bicycles.csv');
                $bikesArray = $parser->parse();

                $bikes = Bicycle::find_all();
                ///echo '<pre>';print_r($bikes);echo '</pre>';
                //$bike = new Bicycle('Totto', '201x', 2018, 'Nuevos productos de Totto', Bicycle::CATEGORY[4], 'Black', Bicycle::GENDER[2], 204.54, 5);

                foreach ($bikes as $bike) :
                    //$bike = (object)$bike;
                    //$bike = new Bicycle($item['brand'], $item['model'], $item['year'], '', $item['category'], $item['color'], $item['gender'], $item['price'], $item['condition_id']);
                    //$bike->setWeightKg($bike['weight_kg']);
                    ?>
                    <tr>
                        <td><?=$bike->brand; ?></td>
                        <td><?=$bike->model; ?></td>
                        <td><?=$bike->year; ?></td>
                        <td><?=$bike->category; ?></td>
                        <td><?=$bike->gender; ?></td>
                        <td><?=$bike->color; ?></td>
                        <td><?=$bike->getWeightKg().'/'.$bike->getWeightLbs(); ?></td>
                        <td><?=$bike->getCondition(); ?></td>
                        <td><?=$bike->getPrice(); ?></td>
                        <td><a class="action" href="<?php echo url_for('/staff/bicycles/show.php?id=' . h(u($bike->id))); ?>">View</a></td>
                        <td><a class="action" href="<?php echo url_for('/staff/bicycles/edit.php?id=' . h(u($bike->id))); ?>">Edit</a></td>
                        <td><a class="action" href="<?php echo url_for('/staff/bicycles/delete.php?id=' . h(u($bike->id))); ?>">Delete</a></td>
                    </tr>
                    <?php
                endforeach;
            ?>
        </table>
    </div>

</div>

<?php include(SHARED_PATH . '/public_footer.php'); ?>
