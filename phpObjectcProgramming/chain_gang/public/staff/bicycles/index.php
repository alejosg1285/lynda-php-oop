<?php
    require_once('../../../private/initialize.php');

    require_login();
?>

<?php

    use ChainHang\Bicycle;
    use ChainHang\Pagination;

    $currentPage = empty($_GET['page']) ? 1 : $_GET['page'];
    $perPage = 2;
    $totalCount = Bicycle::count_all();

    $pagination = new Pagination($currentPage, $perPage, $totalCount);
    // Find all bicycles;
    //$bicycles = Bicycle::find_all();
    $sql = 'SELECT * FROM bicycles ';
    $sql .= "limit {$perPage} ";
    $sql .= "offset {$pagination->offset()}";

    $bicycles = Bicycle::find_by_sql($sql);

?>
<?php $page_title = 'Bicycles'; ?>
<?php include(SHARED_PATH . '/staff_header.php'); ?>

    <div id="content">
        <div class="bicycles listing">
            <h1>Bicycles</h1>

            <div class="actions">
                <a class="action" href="<?php echo url_for('/staff/bicycles/new.php'); ?>">Add Bicycle</a>
            </div>

            <table class="list">
                <tr>
                    <th>ID</th>
                    <th>Brand</th>
                    <th>Model</th>
                    <th>Year</th>
                    <th>Category</th>
                    <th>Gender</th>
                    <th>Color</th>
                    <th>Price</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                </tr>

                <?php foreach ($bicycles as $bicycle) { ?>
                    <tr>
                        <td><?php echo h($bicycle->id); ?></td>
                        <td><?php echo h($bicycle->brand); ?></td>
                        <td><?php echo h($bicycle->model); ?></td>
                        <td><?php echo h($bicycle->year); ?></td>
                        <td><?php echo h($bicycle->category); ?></td>
                        <td><?php echo h($bicycle->gender); ?></td>
                        <td><?php echo h($bicycle->color); ?></td>
                        <td><?php echo h($bicycle->getPrice()); ?></td>
                        <td>
                            <a class="action"
                               href="<?php echo url_for('/staff/bicycles/show.php?id=' . h(u($bicycle->id))); ?>">View
                            </a>
                        </td>
                        <td>
                            <a class="action"
                               href="<?php echo url_for('/staff/bicycles/edit.php?id=' . h(u($bicycle->id))); ?>">Edit
                            </a>
                        </td>
                        <td>
                            <a class="action"
                               href="<?php echo url_for('/staff/bicycles/delete.php?id=' . h(u($bicycle->id))); ?>">
                                Delete
                            </a>
                        </td>
                    </tr>
                <?php } ?>
            </table>

            <?php
                $url = url_for('/staff/bicycles/index.php');

                echo $pagination->paginationLinks($url);
            ?>

        </div>

    </div>

<?php include(SHARED_PATH . '/staff_footer.php'); ?>