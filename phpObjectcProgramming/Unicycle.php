<?php
    /**
     * Created by PhpStorm.
     * User: Alejo Saa G
     * Date: 29/08/2018
     * Time: 22:14
     */

    namespace App\Unicycle;

    use App\Bicycle\Bicycle;


    class Unicycle extends Bicycle
    {
        protected static $wheels = 1;

        /**
         * Unicycle constructor.
         */
        public function __construct($brand, $model, $year, $description, $weightKb) {
            parent::__construct($brand, $model, $year, $description, $weightKb);
        }
    }