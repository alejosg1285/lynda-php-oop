<?php
    /**
     * Created by PhpStorm.
     * User: Alejo Saa G
     * Date: 25/08/2018
     * Time: 22:59
     */

    namespace App\Bicycle;


    class Bicycle
    {
        private $brand;
        private $model;
        private $year;
        private $description;
        private $weightKg = 0.0;
        public $category;
        protected static $wheels = 2;
        public static $instanceCount = 0;
        const CATEGORY = ['Road', 'Mountain', 'Hybric', 'Cruiser', 'BMX'];

        /**
         * Bicycle constructor.
         *
         * @param       $brand
         * @param       $model
         * @param       $year
         * @param       $description
         * @param float $weightKg
         */
        public function __construct($brand, $model, $year, $description, $weightKg) {
            $this->brand = $brand;
            $this->model = $model;
            $this->year = $year;
            $this->description = $description;
            $this->weightKg = $weightKg;
        }

        public function getName() {
            return "{$this->brand}-v{$this->model}-{$this->year}";
        }

        public function getWeightLbs() {
            $kg = 2.2046226218;

            $weightLbs = $this->weightKg * $kg;

            return "{$weightLbs} Lb";
        }

        public function setWeightLbs($weightLbs) {
            $kg = 2.2046226218;

            $this->weightKg = floatval($weightLbs) / $kg;
        }

        public function getWeightKg() {
            return "{$this->weightKg} Kg";
        }

        public static function wheelsDetail() {
            $cantWheel = static::$wheels === 1 ? 'one wheel' : static::$wheels.' wheels';
            return "It has {$cantWheel}.";
        }

        public static function create($brand, $model, $year, $description, $weightKg) {
            $className = get_called_class();
            $obj = new $className($brand, $model, $year, $description, $weightKg);

            self::$instanceCount++;

            return $obj;
        }
    }