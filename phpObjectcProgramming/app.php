<?php
    /**
     * Created by PhpStorm.
     * User: Alejo Saa G
     * Date: 29/08/2018
     * Time: 22:34
     */

    spl_autoload_register(function ($className) {
        $arrNames = explode('\\', $className, -1);
        $className = end($arrNames);
        if (preg_match('/\A\w+\Z/', $className)) {
            $dirs = ['./'];

            foreach ($dirs as $dir) {
                echo "{$dir}{$className}.php";
                if (file_exists("{$dir}{$className}.php")) {
                    include_once "{$dir}{$className}.php";

                    return;
                }
            }
        }
    });

    use App\Bicycle\Bicycle;
    use App\Unicycle\Unicycle;

    $bike = new Bicycle('Totto', '201x', 2018, 'Nuevos productos de Totto', 4.5);
    $unib = new Unicycle('Shimano', 'Crazy X', 2016, 'Una rueda de locos', 1.6);

    echo "bici: {$bike->getName()}\n";
    echo "uni: {$unib->getName()}\n";

    echo "bici: ".Bicycle::wheelsDetail()."\n";
    echo "uni: ".Unicycle::wheelsDetail()."\n";

    echo 'bike: '.Bicycle::$instanceCount.'\n';
    echo 'uni: '.Unicycle::$instanceCount.'\n';

    $street = Bicycle::create('Honda', 'btt', 2016, 'Bici todo terreno', 3.1);
    $mono = Unicycle::create('Ford', 'Mustang', 2019, 'Super bici', 1.4);

    echo 'bike: '.Bicycle::$instanceCount.'\n';
    echo 'uni: '.Unicycle::$instanceCount.'\n';

    echo 'Categories: '.implode(', ', Bicycle::CATEGORY).'\n';
    $bike->category = Bicycle::CATEGORY[4];
    echo "bike: {$bike->category}\n";